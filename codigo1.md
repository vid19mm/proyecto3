class articulo(models.Model):
    nombre=models.CharField(max_length=64,null=True)
    cantidad=models.IntegerField(null=True)
    slug=models.SlugField(max_length=128,null=True)
    categoria=models.ForeignKey(categorias,on_delete=models.CASCADE,blank=True)
    user=models.ForeignKey(User,on_delete=models.CASCADE,blank=True)
    fecha_creacion=models.DateField(blank=True,null=True)